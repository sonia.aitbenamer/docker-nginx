FROM ubuntu:20.04


# Copy root page
# COPY files/index.html /usr/share/nginx/html/index.html

# Copy basic files
# COPY files/nginx.conf  /etc/nginx/nginx.conf

RUN apt-get update
RUN apt install nginx -y
RUN /etc/init.d/nginx start 
# RUN ufw allow 'Nginx HTTP'

COPY localhost.conf /etc/nginx/conf.d/localhost.conf

EXPOSE 80
#VOLUME ["/usr/share/nginx/html"]

CMD ["nginx","-g", "daemon off;"]
